import numpy as np

from .agent import Agent

class UCB1Agent(Agent):
    def __init__(self, name, num_levers):
        super(UCB1Agent, self).__init__(name, num_levers)
    def choose_lever(self):
        probs = self.get_empirical_probabilities()
        probs += np.array([10 if t == 0 else np.sqrt(2 * np.log(self._total_pulls) / t) for t in self._trials])
        return np.argmax(probs)
