import numpy as np

from .agent import Agent

class ThompsonSamplingAgent(Agent):
    def __init__(self, name, num_levers):
        super(ThompsonSamplingAgent, self).__init__(name, num_levers)

    def choose_lever(self):
        alphas = self._successes
        betas = self._trials - self._successes
        probs = np.array([np.random.beta(alpha + 1, beta + 1) for (alpha, beta) in zip(alphas, betas)])
        return np.argmax(probs)
