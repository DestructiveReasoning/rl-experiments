import numpy as np

from .agent import Agent

class RandomAgent(Agent):
    def __init__(self, name, num_levers):
        super(RandomAgent, self).__init__(name, num_levers)

    def choose_lever(self):
        return np.random.randint(0, self._num_levers)

class EpsilonGreedyAgent(Agent):
    def __init__(self, name, num_levers, epsilon, eps_decay=1.0, eps_decay_interval=1):
        super(EpsilonGreedyAgent, self).__init__(name, num_levers)
        assert(epsilon >= 0.0 and epsilon <= 1.0)
        assert(eps_decay > 0.0 and eps_decay <= 1.0)
        assert(eps_decay_interval > 0)
        self._epsilon = epsilon
        self._eps_decay = eps_decay
        self._eps_decay_interval = eps_decay_interval

    def choose_lever(self):
        probs = self.get_empirical_probabilities()
        return np.argmax(probs) if np.random.uniform() > self._epsilon else np.random.randint(0, self._num_levers)

    def update(self, lever, reward):
        super(EpsilonGreedyAgent, self).update(lever, reward)
        if not self._total_pulls % self._eps_decay_interval:
            self._epsilon *= self._eps_decay

class BoltzmannAgent(Agent):
    def __init__(self, name, num_levers, temp, temp_decay=1.0, temp_decay_interval=1):
        super(BoltzmannAgent, self).__init__(name, num_levers)
        assert(temp > 0.0)
        assert(temp_decay > 0.0 and temp_decay <= 1.0)
        assert(temp_decay_interval > 0)
        self._temp = temp
        self._temp_decay = temp_decay
        self._temp_decay_interval = temp_decay_interval

    def choose_lever(self):
        probs = self.get_empirical_probabilities()
        probs = softmax(probs, temp=self._temp)
        return np.random.choice(range(self._num_levers), p=probs)

    def update(self, lever, reward):
        super(BoltzmannAgent, self).update(lever, reward)
        if not self._total_pulls % self._temp_decay_interval:
            self._temp *= self._temp_decay


def softmax(logits, temp=1.0):
    exps = np.exp((logits - np.max(logits)) / temp)
    return exps / exps.sum()
