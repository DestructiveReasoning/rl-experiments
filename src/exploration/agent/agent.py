import numpy as np

class Agent(object):
    def __init__(self, name, num_levers):
        self._num_levers = num_levers
        self._name = name
        self.reset()

    def reset(self):
        self._successes = np.zeros(self._num_levers)
        self._trials = np.zeros(self._num_levers)
        self._total_pulls = 0

    def update(self, lever, reward):
        self._total_pulls += 1
        self._trials[lever] += 1
        if reward == 1.0:
            self._successes[lever] += 1

    def choose_lever(self):
        raise NotImplementedError

    def get_empirical_probabilities(self):
        return np.array([1.0/self._num_levers if t == 0 else s/t for (s, t) in zip(self._successes, self._trials)])

    def get_name(self):
        return self._name
