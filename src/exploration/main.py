import matplotlib.pyplot as plt
import numpy as np
import sys

from agent.baselines import RandomAgent
from agent.baselines import EpsilonGreedyAgent
from agent.baselines import BoltzmannAgent
from agent.ucb import UCB1Agent
from agent.thompson import ThompsonSamplingAgent

from bernoullibandit import BernoulliBandit

NUM_LEVERS = 10             # Amount of arms of the multi-armed bandit
NUM_TRIALS = 50             # Amount of times the entire experiment is repeated
NUM_PULLS = 10000           # Amount of timesteps (arm pulls per agent) per trial


def main():
    bandit_reset_prob = 0.0 # Probability of the bandit resetting its reward probabilities randomly
    config = parse_args()
    if "bandit reset prob" in config.keys():
        bandit_reset_prob = config["bandit reset prob"]

    bandit = BernoulliBandit(NUM_LEVERS, reset_prob=bandit_reset_prob)
    agents = []
    agents.append(RandomAgent("Random Agent", NUM_LEVERS))
    agents.append(EpsilonGreedyAgent("Dumb e-Greedy (e=0.2)", NUM_LEVERS, 0.2))
    agents.append(EpsilonGreedyAgent("e-Greedy (e=0.2, decay=0.9)", NUM_LEVERS, 0.2, eps_decay=0.9))
    agents.append(BoltzmannAgent("Dumb Boltzmann (t=0.5)", NUM_LEVERS, 0.5))
    agents.append(BoltzmannAgent("Boltzmann (t=0.5, decay=0.9)", NUM_LEVERS, 0.5, temp_decay=0.9))
    agents.append(UCB1Agent("UCB-1", NUM_LEVERS))
    agents.append(ThompsonSamplingAgent("Thompson Sampling", NUM_LEVERS))

    regrets = {a.get_name(): np.zeros(NUM_PULLS) for a in agents}

    for trial in range(NUM_TRIALS):
        print("Beginning trial %4d of %4d"%(trial + 1, NUM_TRIALS))

        for a in agents:
            a.reset()

        for pull in range(NUM_PULLS):
            optimal = bandit.get_optimal_reward()
            for a in agents:
                lever = a.choose_lever()
                reward = bandit.pull(lever)
                a.update(lever, reward)
                regrets[a.get_name()][pull] += (optimal - reward) / NUM_TRIALS
            bandit.update()

    regret_data = []
    legend = []
    for a in agents:
        regret_data.append(np.cumsum(regrets[a.get_name()]))
        legend.append(a.get_name())
    show_plots(regret_data, labels=legend, xlabel="Episode", ylabel="Average Regret")

def show_plots(data, xlabel=None, ylabel=None, title=None, labels=None):
    plt.xticks([1000 * i for i in range(int(NUM_PULLS/1000) + 1)])
    plt.grid()
    color_indices = np.linspace(0, 1, len(data))
    for (i, plot) in enumerate(data):
        plt.plot(plot, color=plt.cm.coolwarm(color_indices[i]))

    if labels is not None:
        plt.legend(labels)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    if title is not None:
        plt.title(title)
    plt.show()

def parse_args():
    config = {}
    if len(sys.argv) > 1:
        try:
            bandit_reset_prob = float(sys.argv[1])
            if bandit_reset_prob > 1.0 and bandit_reset_prob <= 100.0:
                print("WARNING: bandit reset probability is above 1.0 - converting to percentage")
                bandit_reset_prob /= 100.0
                print("bandit reset probability will be %.4f"%(bandit_reset_prob))
            elif bandit_reset_prob < 0.0 or bandit_reset_prob > 100.0:
                show_help()
                raise Exception("Invalid bandit reset probability value")
            config["bandit reset prob"] = bandit_reset_prob
        except Exception:
            show_help()
            raise Exception("Invalid bandit reset probability value")
    return config

def show_help():
    print("USAGE: python {} [bandit reset probability]".format(sys.argv[0]))

if __name__ == "__main__":
    main()
