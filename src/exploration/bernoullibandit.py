import numpy as np

class BernoulliBandit(object):
    def __init__(self, num_levers, reset_prob=0.0):
        self._num_levers = num_levers
        self._reset_prob = reset_prob
        self._reset()

    def pull(self, lever):
        return 1.0 if np.random.uniform() < self._probs[lever] else 0.0

    def update(self):
        if np.random.uniform() < self._reset_prob:
            self._reset()

    def get_optimal_reward(self):
        return self._optimal_prob

    def _reset(self):
        self._probs = np.random.uniform(size=self._num_levers)
        self._optimal_prob = np.max(self._probs)
