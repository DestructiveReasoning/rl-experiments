import gym

from agent import QLearningAgent
from agent import QLearningAgentWithExpReplay

from main import start_experiment

EPSILON = 0.8
EPSILON_DECAY = 0.95
GAMMA = 0.99
EXP_BUFFER_SIZE=10000

def main():
    env = gym.make("MountainCar-v0")
    state_dim = env.observation_space.shape
    n_actions = env.action_space.n

    agents = []
    agents.append(QLearningAgent(EPSILON, GAMMA, state_dim, n_actions, eps_decay=EPSILON_DECAY, learning_rate=1e-3))
    agents.append(QLearningAgentWithExpReplay(EPSILON, GAMMA, state_dim, n_actions, eps_decay=EPSILON_DECAY, exp_buffer_size=EXP_BUFFER_SIZE, learning_rate=1e-3))
    start_experiment(env, agents, num_epochs=200, win_condition=-50)

if __name__ == "__main__":
    main()
