import datetime
import gym
import math
import matplotlib.pyplot as plt
import numpy as np
import time

from agent import QLearningAgent
from agent import QLearningAgentWithExpReplay

EPSILON = 0.2
EPSILON_DECAY = 0.99
GAMMA = 0.99
EXP_BUFFER_SIZE=10000

def main():
    env = gym.make("CartPole-v0").env
    state_dim = env.observation_space.shape
    n_actions = env.action_space.n

    agents = []
    agents.append(QLearningAgent(0.1, GAMMA, state_dim, n_actions, eps_decay=EPSILON_DECAY))
    agents.append(QLearningAgentWithExpReplay(EPSILON, GAMMA, state_dim, n_actions, eps_decay=EPSILON_DECAY, exp_buffer_size=EXP_BUFFER_SIZE))
    start_experiment(env, agents, win_condition=800)

def start_experiment(env, agents, win_condition=None, num_epochs=100, checkpoint_epochs=[10, 20, 40, 80]):
    reward_graphs = []

    for agent in agents:
        agent.activate()
        reward_graphs.append(train(agent, env, win_condition=win_condition, n_epochs=num_epochs, checkpoint_epochs=checkpoint_epochs))
        ans = input("Would you like to watch a simulation? (y/n) ")
        while len(ans) == 0 or (ans[0] != 'n' and ans[0] != 'N'):
            filepath = "./videos/{}-final-{}".format(agent.name, math.floor(time.time()))
            last_reward = save_session(agent, env, filepath, watch=True)
            print("Simulation reward: " + str(last_reward))
            ans = input("Would you like to watch a simulation? (y/n) ")

    plot_ci_graphs(agents, reward_graphs)

def train(agent, env, n_epochs=100, print_every=10, win_condition=None, checkpoint_epochs=[]):
    start = datetime.datetime.now()
    mean_reward = 0
    means = []
    highs = []
    lows = []
    for i in range(n_epochs):
        rewards = [run_session(agent, env, train=True) for _ in range(100)]
        mean_reward = np.mean(rewards)
        high_reward = np.percentile(rewards, 75)
        low_reward = np.percentile(rewards, 25)
        means.append(mean_reward)
        highs.append(high_reward)
        lows.append(low_reward)
        agent.decay_eps()

        if not i % print_every:
            elapsed = datetime.datetime.now() - start
            print("epoch %4d | mean reward: %4.4f\teps: %.4f | elapsed time: %3.2f mins"%(i, mean_reward, agent.check_epsilon(), elapsed.seconds/60))
        if win_condition is not None and mean_reward >= win_condition:
            print("You win! Reached mean reward of {}".format(mean_reward))
            break
        if i in checkpoint_epochs:
            filepath = "./videos/{}-epoch-{}-{}".format(agent.name, i, math.floor(time.time()))
            save_session(agent, env, filepath)
    print()
    elapsed = datetime.datetime.now() - start
    print("Training finished in %.2f minutes"%(elapsed.seconds/60))
    print("Last earned mean reward from training: " + str(mean_reward))
    return (means, highs, lows)

def save_session(agent, env, filepath, watch=False, t_max=1000):
    m = gym.wrappers.Monitor(env, filepath)
    print(">>>> Saving simulation video to {}...".format(filepath))
    return run_session(agent, m, watch=watch, t_max=t_max)

def run_session(agent, env, train=False, watch=False, t_max=1000):
    total_reward = 0
    s = env.reset()
    for t in range(t_max):
        a = agent.get_action(s)
        next_s, r, done, _ = env.step(a)
        total_reward += r
        if train:
            agent.update(s, a, r, next_s, done)
        if watch:
            env.render()
        s = next_s
        if done:
            break
    if watch:
        env.close()
    return total_reward

def plot_ci_graphs(agents, reward_graphs):
    plt.xlabel("Epoch")
    plt.ylabel("Mean reward")
    color_indices = np.linspace(0, 1, len(agents))
    for i in range(len(agents)):
        graph = reward_graphs[i]
        agent = agents[i]
        color = plt.cm.plasma(color_indices[i])
        plt.plot(graph[0], label=agent.name, color=color)
        plt.fill_between(range(len(graph[0])), graph[2], graph[1], alpha=0.4, color=color)
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()
