import numpy as np
import random

class ExpReplayBuffer(object):
    def __init__(self, storage):
        self._storage = storage
        self._buffer = []
        self._index = 0

    def add(self, s, a, r, next_s, done):
        data = (s, a, r, next_s, done)
        if len(self._buffer) < self._storage:
            self._buffer.append(data)
        else:
            self._buffer[self._index] = data
        self._index = (self._index + 1) % self._storage

    def sample(self, batch_size):
        indices = [random.randint(0, len(self._buffer) - 1) for _ in range(batch_size)]
        s_batch = np.array([self._buffer[i][0] for i in indices])
        a_batch = np.array([self._buffer[i][1] for i in indices])
        r_batch = np.array([self._buffer[i][2] for i in indices])
        next_s_batch = np.array([self._buffer[i][3] for i in indices])
        done_batch = np.array([self._buffer[i][4] for i in indices])
        return s_batch, a_batch, r_batch, next_s_batch, done_batch
