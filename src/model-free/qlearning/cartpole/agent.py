import keras
import keras.layers as L
import numpy as np
import random
import tensorflow as tf

from expreplay import ExpReplayBuffer

class QLearningAgent(object):
    def __init__(self, epsilon, gamma, state_dim, n_actions, eps_decay=1.0, learning_rate=1e-4):
        self.name = "QLearningAgent"
        self._epsilon = epsilon
        self._epsilon_decay = eps_decay
        self._gamma = gamma
        self._learning_rate = learning_rate
        self._state_dim = state_dim
        self._n_actions = n_actions

        self._sess = tf.Session()

        self._states_ph = None
        self._actions_ph = None
        self._rewards_ph = None
        self._next_s_ph = None
        self._done_ph = None

        self._network = keras.models.Sequential()

        # Symbolic predicted q values in current state
        self._predicted_qvalues = None
        # Symbolic predicted q values for the actions taken
        self._predicted_action_vals = None
        self._predicted_next_qvalues = None
        self._next_state_values = None
        self._target_qvalues = None
        self._target_qvalues = None

        self._loss = None
        self._trainer = None

    def get_action(self, state):
        predicted_qvalues = self._network.predict(state[None])[0]
        optimal_action = np.argmax(predicted_qvalues)
        if random.random() < self._epsilon:
            return random.randint(0, self._n_actions - 1)
        return optimal_action

    def update(self, s, a, r, next_s, done):
        s, a, r, next_s, done = self._next_datapoint(s, a, r, next_s, done)
        self._sess.run(self._trainer, feed_dict={self._states_ph: s, self._actions_ph: a, self._rewards_ph: r, self._next_s_ph: next_s, self._done_ph: done})

    def activate(self):
        tf.reset_default_graph()
        self._sess = tf.Session()
        keras.backend.set_session(self._sess)

        self._states_ph = keras.backend.placeholder(dtype='float32', shape=(None,) + self._state_dim)
        self._actions_ph = keras.backend.placeholder(dtype='int32', shape=[None])
        self._rewards_ph = keras.backend.placeholder(dtype='float32', shape=[None])
        self._next_s_ph = keras.backend.placeholder(dtype='float32', shape=(None,) + self._state_dim)
        self._done_ph = keras.backend.placeholder(dtype='bool', shape=[None])

        self._init_network()

        # Symbolic predicted q values in current state
        self._predicted_qvalues = self._network(self._states_ph)
        # Symbolic predicted q values for the actions taken
        self._predicted_action_vals = tf.reduce_sum(self._predicted_qvalues * tf.one_hot(self._actions_ph, self._n_actions), axis=1)
        self._predicted_next_qvalues = self._network(self._next_s_ph)
        self._next_state_values = keras.backend.max(self._predicted_next_qvalues, axis=1)
        self._target_qvalues = self._rewards_ph + self._gamma * self._next_state_values
        self._target_qvalues = tf.where(self._done_ph, self._rewards_ph, self._target_qvalues)

        self._loss = (self._predicted_action_vals - tf.stop_gradient(self._target_qvalues))**2
        self._loss = tf.reduce_mean(self._loss)

        self._trainer = tf.train.AdamOptimizer(self._learning_rate).minimize(self._loss)

    def decay_eps(self):
        self._epsilon *= self._epsilon_decay

    def check_epsilon(self):
        return self._epsilon

    def _next_datapoint(self, s, a, r, next_s, done):
        return [s], [a], [r], [next_s], [done]

    def _init_network(self):
        self._network.add(L.InputLayer(self._state_dim))
        self._network.add(L.Dense(200, activation='relu'))
        self._network.add(L.Dense(200, activation='relu'))
        self._network.add(L.Dense(self._n_actions))

class QLearningAgentWithExpReplay(QLearningAgent):
    def __init__(self, epsilon, gamma, state_dim, n_actions, eps_decay=1.0, learning_rate=1e-4, exp_buffer_size=1000, update_every=5):
        super(QLearningAgentWithExpReplay, self).__init__(epsilon, gamma, state_dim, n_actions, eps_decay, learning_rate=learning_rate)
        self.name = "QLearningAgentWithExpReplay"
        self._replay = ExpReplayBuffer(exp_buffer_size)
        self._update_every = update_every
        self._tick = 0

    def update(self, s, a, r, next_s, done):
        self._replay.add(s, a, r, next_s, done)
        if self._tick == 0:
            super(QLearningAgentWithExpReplay, self).update(s, a, r, next_s, done)
        self._tick = (self._tick + 1) % self._update_every

    def _next_datapoint(self, s, a, r, next_s, done):
        return self._replay.sample(32)
