module Router exposing (getFragment)

import Url
import Url.Parser as Parser exposing (Parser, map, top, string, fragment, (</>), oneOf)
import Tuple

--type Route = Home | About | Exploration | ModelFree | Listing | Other Url.Url

type AbstractRouteType = RootFragment | External

type alias Docs = (String, Maybe String)

docs : Parser (Docs -> a) a
docs =
  map Tuple.pair (string </> fragment identity)

--getFragment : Url.Url -> RouteType
--getFragment url = let snd = Tuple.second(Maybe.withDefault ("", Just (Url.toString url)) (Parser.parse docs url))
--                  in Maybe.withDefault (Other (Url.toString url)) (Maybe.map Fragment snd)

getFragment : Url.Url -> Maybe String
getFragment url =
  case Parser.parse parser url of
    Just RootFragment -> Just (getFragment_ url)
    _ -> Nothing

getFragment_ : Url.Url -> String
getFragment_ url =
  let
    urlStr = Url.toString(url)
    splits = String.split "#" urlStr
  in Maybe.withDefault "home" (Maybe.andThen List.head (List.tail splits))

parser : Parser (AbstractRouteType -> a) a
parser = oneOf
          [ map (RootFragment) top ]

--fromUrl : Url.Url -> Route
--fromUrl url =
--  Maybe.withDefault (Other url) (Parser.parse parser url)
