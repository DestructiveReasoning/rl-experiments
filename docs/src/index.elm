module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Url

import NavBar exposing (..)
import Card exposing (..)
import Router exposing(..)

main : Program () Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }

type Page = Home | About | Exploration | ModelFree | TooSoon

type alias Model =
  { key : Nav.Key
  , url : Url.Url
  , page: Page
  , debug: String
  }

init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
  let page = Maybe.withDefault Home (pageFromUrl url)
  in ( Model key url page "", Cmd.none )

type Msg
  = LinkClicked Browser.UrlRequest
  | UrlChanged Url.Url

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    LinkClicked urlRequest ->
      case urlRequest of
        Browser.Internal url ->
          ( model, Nav.pushUrl model.key (Url.toString url) )
        Browser.External href ->
          ( model, Nav.load href )
    UrlChanged url ->
      case pageFromUrl url of
        Just p -> ( { model | url = url, page=p }, Cmd.none )
        _ -> ( { model | url = url }, Nav.load (Url.toString url) )

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

view : Model -> Browser.Document Msg
view model =
  { title = "RL Experiments"
  , body = 
      [ navBar 
          [ NavBarLabel "Articles" "/#articles"
          , NavBarImage "res/logo.png" "/#home"
          , NavBarLabel "About" "/#about"
          ]
--      , text "The current URL is: "
--      , b [] [ text (Url.toString model.url) ]
--      , br [] []
--      , b [] [ text (model.debug) ]
--      , ul []
--        [ viewLink "/home"
--        , viewLink "/profile"
--        , viewLink "/reviews/the-century-of-the-self"
--        , viewLink "/reviews/public-opinion"
--        , viewLink "/reviews/shah-of-shahs"
--        , viewLink "#yoyoma"
--        , viewLink "#bababooey"
--        ]
      , renderModel model
      ]
  }

renderModel : Model -> Html msg
renderModel model =
  case model.page of
    Exploration -> renderExploration
    ModelFree -> renderModelFree
    Home -> renderHome
    TooSoon -> renderTooSoon
    _ -> renderHome

renderExploration : Html msg
renderExploration =
  div [class "exploration-content"] [
    h1 [] [text "Exploration and Exploitation"]
  , createCard {
      link="/exploration/multi-armed-bandit.html"
    , title="Multiarmed Bandits"
    , desc="Background description of the Multiarmed Bandit problem."
    , color="#444477"
    , img="res/multiarmedbandit.png"
    }
  ]

renderModelFree : Html msg
renderModelFree =
  div [class "modelfree-content"] [
    h1 [] [text "Model-Free Learning"]
  , createCard {
      link="/model-free/qlearning/cartpole.html"
    , title="Approximate Q-Learning"
    , desc="Solving control problems in continuous state space with (deep) Q-Learning."
    , color="#bb6688"
    , img="res/qlearning.png"
    }
  ]

renderHome : Html msg
renderHome =
  div [class "home-content"] [
      h1 [] [text "Reinforcement Learning Experiments"]
    , createCard {
          link="#exploration"
        , title="Exploration & Exploitation"
        , desc=
            """
            Studying how agents can explore their environment to adapt
            and discover more optimal strategies, and analyzing the tradeoff
            between exploring new strategies and exploiting those that are
            already developed.
            """
        , color="#8822aa"
        , img="res/exploration.png"
      }
    , createCard {
          link="#modelfree"
        , title="Model-Free Learning"
        , desc="Learning to master tasks without any prior knowledge of the environment."
        , color="#ff6666"
        , img="res/modelfree.png"
      }
  ]

renderTooSoon : Html msg
renderTooSoon =
  div [class "toosoon-content"] [
    h1 [] [text "Coming Soon..."]
  , img [class "logo-inverted", src "res/logo.png"] []
  ]

pageFromUrl : Url.Url -> Maybe Page
pageFromUrl url =
  case getFragment url of
    Just "exploration" -> Just Exploration
    Just "home" -> Just Home
    Just "modelfree" -> Just ModelFree
    Just s -> Just TooSoon
    _ -> Nothing

viewLink : String -> Html msg
viewLink path = li [] [ a [ href path ] [ text path ] ]
