module Card exposing (createCard, CardRecord)

import Html exposing (..)
import Html.Attributes exposing (..)

type alias CardRecord = 
  {
      link : String
    , img : String
    , title : String
    , desc : String
    , color : String
  }

createCard : CardRecord -> Html msg
createCard card = 
  div [class "card"] [
    a [href card.link] [
        img [src card.img, style "background" card.color] []
      , div [class "card-text"] [
            h1 [] [text card.title]
          , p [class "card-desc"] [text card.desc]
        ]
    ]
  ]
