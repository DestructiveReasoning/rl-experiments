module NavBar exposing (NavBarEntry(..), navBar)

import Html exposing (..)
import Html.Attributes exposing (..)

type NavBarEntry = NavBarLabel String String | NavBarImage String String

navBar : (List NavBarEntry) -> Html msg
navBar entries = div [class "navbar"] [
                    ul [] (List.map ((\x -> li [] [ x ] ) << renderEntry) entries)
                 ]

renderEntry : NavBarEntry -> Html msg
renderEntry entry =
  case entry of
    NavBarLabel labelText labelRef -> a [href labelRef] [p [] [text labelText]]
    NavBarImage imgSrc imgRef -> a [href imgRef] [img [src imgSrc] []]
