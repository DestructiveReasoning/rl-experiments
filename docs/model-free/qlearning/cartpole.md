---
title: Controlling an Inverted Pendulum with Approximate Q-Learning
link-citations: true
---

We'll now try to solve the classic control problem of balancing an inverted
pendulum on a cart using reinforcement learning methods. In essence, this
involves controlling a motor on the cart to move it along one dimension to stop
the pendulum from following over, like so:

![](./videos/QLearningAgentWithExpReplay-epoch-10.webp)

Err... hopefully a little better than that.

Solving this problem using control theory usually requires an extensive physical
model of the system, which requires lots of problem-specific math and physics
analysis, and... well, we can't all be [Richard
Stallman](https://stallman.org/biographies.html). With the methods presented
here, no model of the system is needed at all, and thus the algorithms we will
design should be able to learn how to control a plethora of systems without
really changing any of the code.

## Markov Decision Processes

Before we begin designing an algorithm that can learn to solve this problem, we
must formalize a model of how our AI agent interacts with the environment that
it finds itself in. In reinforcement learning, we generally express this as a
*Markov Decision Process* (MDP) (@suttonbarto). With this model, at each
timestep, an agent finds itself in some fully-observable state, and must select
an action to perform. Based on the agent's state and chosen action, the agent
receives a reward, and its state changes. Generally, the goal is to maximize the
expected cumulative reward that the agent receives over some time horizon.

![Block Diagram of an MDP](graphs/mdp.png){width=500px height=333px}

More formally, an MDP is expressed as a 3-tuple $(\mathcal{S}, \mathcal{A},
\mathcal{P})$, where $\mathcal{S}$ is a set of states (potentially of infinite
cardinality), $\mathcal{A}$ is a set of actions (which will be finite for the
purpose of this example), and $\mathcal{P}$ is a probability kernel that assigns
a probability distribution over states and rewards to state-action pairs
(@algorithms-for-rl). Thus, the agent transitions to state $S\in\mathcal{S}$
and earns a reward $R\in\mathbb{R}$ upon taking action $a\in\mathcal{A}$ from
state $s\in\mathcal{S}$ according to:

$$ (S(s,a), R(s,a))\sim \mathcal{P}(\cdot | s, a) $$

We can then write the expected immediate reward for taking action $a$ in state
$s$ as 

$$ r(s, a)\triangleq\mathbb{E}[R(s,a)] $$

However, it does not behoove us to decide on actions to take based on the
immediate reward alone. Instead, we aim to maximize the expected cumulative
return. At timestep $t$, we express the remaining expected cumulative return
$G_t$ as

$$ G_t\triangleq\sum_{k=0}^{\infty}\gamma^kR_{t + k + 1} $$

where $R_t$ is the immediate reward at timestep $t$, and $\gamma\in[0,1)$ is a
discount factor that prevents the return from diverging.

The agent's job is to figure out some policy $\pi(a|s)$ that provides a
probability distribution over actions to take in a given state. Ideally, this
policy will be tuned over time so that it allows the agent to receive the
greatest return. In order to reason about learning such a policy, we consider
the agent's *value function*, which is a function that represents the expected
return received by an agent following some policy $\pi$:

$$ V^\pi(s)\triangleq\mathbb{E}_{A_t\sim\pi(a|s)}\left[G_t\rvert S_t=s\right] =
\mathbb{E}_{A_t\sim\pi(a|s)}\left[\sum_{k=0}^{\infty}\gamma^kR_{t+k+1}\bigg\rvert
S_t=s\right]$$

where $S_t$ and $A_t$ are the agent's state and selected action at timestep $t$,
respectively.

We also consider the agent's *action-value function* $Q^\pi(s,a)$, which is similar to the
value function, but represents the expected return upon taking a given action in
a given state:

$$ Q^\pi(s, a)\triangleq\mathbb{E}_{A_t\sim\pi(a|s)}[G_t\rvert S_t=s, A_t=a] =
\mathbb{E}_{A_t\sim\pi(a|s)}\left[\sum_{k=0}^\infty\gamma^kR_{t+k+1}\bigg\rvert
S_t=s, A_t=a\right] $$

Moreover, and (very) importantly, we can write these functions in a recursive
manner. Expanding the expectation in the value function gives

$$
\begin{aligned}
V^\pi(s) &= \mathbb{E}_{A_t\sim\pi(a|s)}[R_{t+1} + \gamma G_{t+1}\rvert S_t=s]\\
         &=
         \sum_{a\in\mathcal{A}}\pi(a|s)\sum_{s'\in\mathcal{S}}\sum_{r\in\mathbb{R}}\mathcal{P}(s',
             r | s, a)\left(r + \gamma\mathbb{E}_{A_t\sim\pi(a|s)}[G_{t+1}\rvert
           S_{t+1}=s']\right)\\
         &=
         \sum_{a\in\mathcal{A}}\pi(a|s)\sum_{s'\in\mathcal{S}}\sum_{r\in\mathbb{R}}\mathcal{P}(s',
             r | s, a)\left(r + \gamma V^\pi(s')\right)
\end{aligned}
$$

This form is called the Bellman equation for the value function (@suttonbarto).

Now let us consider the optimal value and action-value functions. For an optimal
policy, we must have

$$
\begin{aligned}
  V^*(s) &= \max_\pi V^\pi(s) && \forall s\in\mathcal{S}\\
  Q^*(s, a) &= \max_\pi Q^\pi(s,a) && \forall (s, a)\in\mathcal{S}\times\mathcal{A}\\
\end{aligned}
$$

where $V^*, Q^*$ are the optimal value and action-value functions, respectively.
This follows because an optimal policy should always pick the state with the
highest expected value. Note that the optimal value function therefore can also
be expressed as

$$ V^*(s) = \max_{a\in\mathcal{A}}Q^*(s, a) $$

Now let's look into the optimal action-value function, $Q^*(s,a)$. Expanding on
the definition of the action-value function, we get

$$
\begin{aligned}
  Q^\pi(s,a) &= \mathbb{E}\left[\sum_{k=0}^\infty\gamma^kR_{t+k+1}\bigg\rvert S_t=s,
  A_t=a\right]\\
      &= \mathbb{E}\left[R_{t+1} + \sum_{k=1}^\infty\gamma^k
      R_{t+k+1}\bigg\rvert S_t=s, A_t=a\right]\\
      &= \mathbb{E}\left[R_{t+1} + \gamma\sum_{k=0}^\infty\gamma^k
      R_{t+k+2}\bigg\rvert S_t=s, A_t=a\right]\\
      &= \mathbb{E}\left[R_{t+1} + \gamma V^\pi(S_{t+1})\bigg\rvert S_t=s,
      A_t=a\right]\\
  \therefore Q^*(s,a) &= \mathbb{E}\left[R_{t+1} + \gamma
  V^*(S_{t+1})\bigg\rvert S_t=s, A_t=a\right]\\
      &= \sum_{s'\in\mathcal{S}}\sum_{r\in\mathbb{R}}\mathcal{P}(s', r | s,
          a)\left[r + \max_{a'\in\mathcal{A}}Q_*(s', a')\right]\\
\end{aligned}
$$

This is called the Bellman Optimality equation for the action-value function
(@suttonbarto). Note that this equation is essentially an expectation of a
quantity over states and rewards.

## Q-Learning

What we saw in the previous section suggests that if an agent can estimate the
action-value function well, it can infer an approximately optimal policy.
Unfortunately, the Bellman Optimality equation for the action-value function is
intractable, as it computes sums over the entire state space and over all real
numbers. However, the equation is an expectation, we can approximate its value
by sampling. For example, we may start by initializing $Q(s,a)$ with zeros for
all $s$ and $a$. The agent starts in state $s_0$, chooses an action $a_0$ at
random (since all action values at this point are equal), and then transitions
to state $s_1$ upon receiving reward $r_1$. We approximate $Q(s_0, a_0)\approx
Q'(s_0, a_0)$ as 

$$ Q'(s_0, a_0) = r_1 + \gamma\max_{a\in\mathcal{A}}Q(s_1, a) $$

where $Q(s_1, a)$ is the current estimate of the action-value function. Then, we
may update the current estimate of $Q(s_0, a_0)$ according to

$$ Q(s_0, a_0) \leftarrow (1-\alpha)Q(s_0, a_0) + \alpha Q'(s_0, a_0) $$

where $\alpha\in[0,1]$ can be interpreted as the learning rate.

Unfortunately, this method may still prove to be intractable in some
circumstances. Note that as currently described, a value $Q(s,a)$ is needed for
every pair $(s,a)\in\mathcal{S}\times\mathcal{A}$. For very large state or
action spaces, this might require a prohibitively large amount of memory. Even
worse, if the state or action spaces are *continuous*, storing these $Q$-values
becomes impossible altogether. This is the case for the inverted pendulum
problem described above: the state space consists of the position and velocity
of the cart, as well as the angle and angular velocity of the pendulum, all of
which are continuous variables. To circumvent this, we replace the tabular
representation of the action-value function with a function approximator, which
will be implemented as a neural network for the purpose of this example.

In order to train the neural network, we must define a loss function.
Essentially, in order to improve the neural network, it must produce $Q$-values
that become closer and closer to their "true" values. This is therefore a
regression problem, however since the "true" $Q$-values are unknown, there is no
target data. The mean squared error loss can be written as follows:

$$ \mathcal{L}(\theta)\triangleq\frac{1}{N}\sum_{n=1}^N\left[Q(s_n, a_n) -
    \left(r_{n+1} + \gamma\max_{a'\in\mathcal{A}}Q(s_{n+1}, a')\right)\right]^2 $$

where $\theta$ is the parameters of the function approximator, $N$ is the amount
of samples (usually 1), $s_n, a_n$ are the state and action of the $n$th sample
respectively, and $r_{n+1}, s_{n+1}$ are the reward received and state
transitioned to upon taking action $a_n$, respectively. For convenience, we'll
call the $r_{n+1} + \gamma \max_{a'}Q(s_{n+1}, a')$ term $\hat{Q}(s, a)$, so the
loss can be written as 

$$ \mathcal{L}(\theta) = \frac{1}{N}\sum_{n=1}^N\left(Q(s_n, a_n) - \hat{Q}(s_n,
    a_n)\right)^2 $$

We must now derive the gradient of this loss with respect to $\theta$ to
determine how to update the function approximator. Taking the gradient of the
loss directly will be unstable, since both the $Q(s_n,a_n)$ and
$\hat{Q}(s_n,a_n)$ terms depend on $\theta$. To improve stability, we must treat
one of these as constant in $\theta$, like as if it's a target value. This
gradient is called a *semi-gradient* (@suttonbarto). We choose
to treat $\hat{Q}(s_n, a_n)$ as a constant in the gradient calculations, so we
have

$$
\begin{aligned}
\nabla_\theta\mathcal{L}(\theta) =
\frac{2}{N}\sum_{n=1}^N\left(Q(s_n,a_n)-\hat{Q}(s_n,a_n)\right)\nabla_\theta
Q(s_n, a_n)
\end{aligned}
$$

Most automatic differentiation libraries provide functions for neglecting
gradients of nodes, like TensorFlow's `tf.stop_gradient()`, for example. This
can be used to treat $\hat{Q}$ as constant in $\theta$.

### Off-Policy Learning with Experience Replay

The parameter updates described above look quite similar to parameter updates in
a standard supervised regression context. However, in supervised learning,
convergence and stability guarantees usually assume that the data is i.i.d. In
our example, if the data is taken step by step from the simulation, it is
certainly *not* i.i.d. Here, we present a very simple enhancement to Q-Learning
that both improves its stability and speeds up training.

To improve performance, we may introduce an "Experience Replay Buffer"
(@lin1993reinforcement), which is essentially a queue of tuples $(S, A, R, S',
D)$, where $S\in\mathcal{S}$ is a state, $A\in\mathcal{A}$ is the action
taken in state $S$ for the corresponding datapoint, $R\in\mathbb{R}$ is the
reward received, $S'\in\mathcal{S}$ is the state that is visited upon taking
action $A$, and $D\in\texttt{\{true, false\}}$ indicates if the corresponding
state transition led to the end of the simulation. Instead of using consecutive
datapoints from the simulation steps to train the function approximator, we
simply sample $N$ datapoints from the buffer (say, at random), and train the
network with those. This is called *off-policy learning*, because the agent does
not learn with samples that are taken in sequence according to the decisions
made by its policy. At each step of the simulation, the agent enqueues its
observed $(S, A, R, S', D)$ tuple to the experience replay buffer. When the
buffer reaches a given maximum size, it dequeues the oldest data.

Not only does using the experience replay buffer improve convergence, but it
also makes much more efficient use of the data that the agent gathers: instead
of each data sample being used once to train the function approximator, the data
can be reused several times. This suggests that agents that make use of an
experience replay buffer may converge faster (@lin1993reinforcement).

## Controlling the Inverted Pendulum

### The CartPole Environment

To experiment with how Q-Learning can be used to control an inverted pendulum,
we use the CartPole environment provided by OpenAI Gym (@openaigym). As
previously described, this environment simulates a cart with an inverted
pendulum mounted on it, and the simulation ends when the pendulum surpasses a
certain angle (representing the event that the pendulum fell) or when the cart's
position is too far from the center. The agent receives a reward of $+1$ for
every step that the simulation runs, and a reward of $0$ when the simulation
ends.

The agent's state space is a subset of $\mathbb{R}^4$, containing the cart's
position and velocity as well as the pendulum's angle and angular velocity. The
action space is binary: at each step, a leftward force or a rightward force is
applied to the cart. Moreover, the environment dynamics are stochastic, which
encourages the policy learned by the agent to be robust.

### Experiments

We observe the performance of Q-Learning on the CartPole environment both with
and without the use of experience replay. Both agents use an $\epsilon$-greedy
policy where $\epsilon$ is decayed by $1\%$ after each epoch. For all
experiments, the discount factor $\gamma$ is set to $0.99$. Moreover, the loss
was minimized using the Adam optimizer with an initial learning rate of
$10^{-4}$.

The function approximator was implemented as a fully-connected feedforward
neural network with two hidden layers, as follows:

* **Input layer**: $n_s$ nodes, where $n_s$ is the dimensionality of the state space
* **Hidden layer 1**: Dense layer with 200 neurons and ReLU activation
* **Hidden layer 2**: Dense layer with 200 neurons and ReLU activation
* **Output layer**: Dense layer with $n_a$ nodes and linear activation, where $n_a$ is the
dimensionality of the action space

Note that no sigmoid or softmax activation is given to the output layer, because
this layer computes the action values for each action.

In each epoch, the agent undergos 100 simulations in the environment. The agent
without any experience replay updates its function approximator at every step of
each simulation. The agent with experience replay has a buffer with a maximum
capacity of 10000 datapoints, and its function approximator is updated every 5
steps using 32 samples selected at random (with replacement) from the experience
replay buffer. All simluations are capped at 1000 timesteps, and training is
stopped when an agent reaches a mean reward of over 800 in an epoch.

The mean rewards obtained from both agents are plotted below.

![Mean rewards achieved during
training](graphs/mean-reward-qlearning.png){width=600px, height=400px}

The shaded regions represent the space between the 25th and 75th percentile of
rewards earned in the given epoch. Note that the graph for the agent using
experience replay stops sooner, indicating that it earned a good enough mean
reward much faster. Unfortunately, stability remains to be an issue even with the
agent using experience replay.

### Demos

Below are sample simulations with both agents after training.

![Simple Q-Learning Agent](videos/QLearningAgent-final.webp){width=400
  height=267}

![Q-Learning Agent with Experience
Replay](videos/QLearningAgentWithExpReplay-final.webp){width=400 height=267}

## An Aside: Generalization to Other Problems

Part of the beauty of reinforcement learning algorithms is that they do not
describe how to solve a particular problem, they describe a method of learning
how to solve problems in general. No code for the Q-Learning agents described
above is particular to the inverted pendulum problem, so these agents should be
able to learn in different environments as well.

To test this, the agents were pitted against a very different task, known as the
MountainCar problem (@openaigym). Here, a car is located in a valley and must reach
a flag at the top of one of the peaks. However, its engine does not provide
enough power for the car to simply climb the hill; rather, the car must start by
going up the other hill to gain enough momentum. This problem is relatively
difficult because the rewards are *sparse*: the agent always receives a reward
of $-1$ unless it reaches the goal, where the reward is $0$. Therefore, in
simulations where the agent doesn't reach the goal, it learns nothing, because
every action it takes yields the same cumulative return.

To mitigate this, we must increase the exploration ratio drastically. In the
following experiments, $\epsilon$ was set to $0.8$, and it decayed by $10\%$
after each epoch to ensure that the agent wouldn't explore too much towards the
end of training. Otherwise, the agents were configured identically to how they
were for the inverted pendulum experiments. The mean rewards for both agents are
shown below:

![Mean rewards received during training on the MountainCar
environment](graphs/mean-reward-qlearning-mountaincar.png){width=600px
  height=400px}

Interestingly, the agent that didn't use experience replay was unable to learn
how to solve the problem, while the agent with experience replay was able to
solve it to a decent degree within relatively few epochs. It is hypothesized
that this is due to the improved efficiency of samples in the experience replay
setting, as each data sample is used for training several times rather than
once. A demonstration of the agent's performance is given below:

![Q-Learning Agent with Experience
Replay](videos/QLearningAgentWithExpReplay-MountainCar-final.webp)

## Next Steps

Although both Q-Learning agents were able to learn to control the inverted
pendulum, it was noted that the progression of the agents' mean rewards through
training were not very stable. To improve this, it would be interesting to
experiment with Double Q-Learning (@doubleq) and Dueling Q-Learning
(@wang2015dueling) algorithms. Moreover, it would be instructive to compare
Q-Learning methods to on-policy methods or policy gradient methods.

The code that was used for implementing the algorithms discussed in this text
can be perused
[here](https://gitlab.com/DestructiveReasoning/rl-experiments/tree/master/src/model-free/qlearning/cartpole).

* * * *

## References
