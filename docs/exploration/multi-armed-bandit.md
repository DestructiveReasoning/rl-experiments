---
title: The Multi-Armed Bandit Problem
link-citations: true
---

## Overview of the problem

The multi-armed bandit problem is an incredibly simple problem that illustrates
a key difference between reinforcement learning and supervised learning. Despite
its simplicity, however, it provides a great environment for experimenting with
and evaluating core components of reinforcement learning algorithms.

The problem can be formulated as follows. Consider a scenario where you are
presented with $k$ slot-machines, and you must repeatedly select a slot machine
to play. The slot machines return rewards, each following a different
probability distribution, all of which are unknown to you. The goal is to
maximize the expected reward that you observe over $T$ slot machine plays.

The challenging part of this problem, of course, is that you do not know the
likelihood of winning rewards among any of the machines. In order to make better
decisions, you must play around with all of the machines in an attempt to
estimate their expected rewards. With good enough estimates of these expected
rewards, it is easy to determine which slot machine should be played at each
timestep: it is simply the machine with the highest expected reward.

However, how could you determine when you have "good enough" estimates of the
expected rewards, and what exactly does "playing around" with the machines
entail?

This highlights the infamous *exploration versus exploitation* problem in
reinforcement learning. In order to play the aforementioned game optimally, a
certain degree of *exploration* of the machines is necessary to form good
estimates of their value, and in order to optimize your expected reward, you
must *exploit* the best slot machine by playing it most often. However, these
exploration and exploitation activities often conflict with one another: if
you're really exploiting the perceived best machine, you are not improving your
estimates of the machines' expected rewards, but if you're exploring with the
machines, you are likely not earning the optimal expected reward at each
timestep. Thus, a tradeoff between exploration and exploitation must be made,
and the implementation of how to handle this tradeoff can drastically affect the
expected reward that is observed.

More formally, the multi-armed bandit problem consists of a scenario where an
agent must repeatedly choose between one of $k$ actions, each of which returns
rewards following a different unknown probability distribution. We define the
value of a given action $a$, denoted by $q_{\star}(a)$, as the expected reward when
action $a$ is selected (@suttonbarto). Thus,
  $$q_{\star}(a) = \mathbb{E}[R_t \rvert A_t=a]$$
where $R_t$ and $A_t$ are the reward observed and the action taken at timestep
$t$, respectively. Throughout the agent's lifetime, it maintains action value
estimates $Q(a)$, that ideally approach $q_\star(a)$ through time.

## Measuring the performance of an agent

In order to compare various agents in the multi-armed bandit setting, we need a
method of quantifying their performance. A common metric for "scoring" the
optimality of actions in this setting is by computing the agents' *total
regret*. We define the regret seen by an agent at some timestep $t$ as the
difference between the true expectation of the optimal reward, and the
expectation of the reward of the action chosen by the agent at timestep $t$. The
total regret $\mathcal{R}(T)$ seen by an agent at timestep $T$ can therefore be
expresed as
  $$\mathcal{R}(T) = \sum_{t=1}^{T}(\hat{q} - q_\star(A_t))$$
where $\hat{q} = \max_a q_\star(a)$ (@vermorel2005multi).

Note that the total regret converges if and only if an agent successfully
determines an optimal action and subsequently proceeds to only perform this
action.

By measuring or estimating the total regret due to various action-selection
policies, we may quantitatively observe their performance in the multi-armed
bandit problem, and determine how well they handle the exploration-exploitation
tradeoff.

* * * *

## References
