# Reinforcement Learning Experiments

This repository will maintain a compendium of reinforcement learning
experiments, whether they be toy examples or fun projects, that I will implement
throughout my journey doing research in this field.

I intend to document every single experiment and project in a blog-like website,
which will (eventually) be hosted at
<https://destructivereasoning.gitlab.io/rl-experiments>.
